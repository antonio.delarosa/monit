from paramecio2.libraries.config_admin import config_admin
from paramecio2.libraries.i18n import I18n

#modules_admin=[[I18n.lang('admin', 'users_admin', 'User\'s Admin'), 'paramecio.modules.admin.admin.ausers', 'ausers']]

config_admin.append([I18n.lang('monit', 'monitoring', 'Monitoring')])

config_admin.append([I18n.lang('monit', 'config', 'Configuration'), 'modules.monit.admin.config', 'admin_app.monit_config'])
config_admin.append([I18n.lang('monit', 'servers', 'Servers'), 'modules.monit.admin.servers', 'admin_app.monit_servers'])
config_admin.append([I18n.lang('monit', 'alerts', 'Alerts'), 'modules.monit.admin.alerts', 'admin_app.monit_alerts'])
