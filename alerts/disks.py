from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries import datetime
import sys
import os
try:
    import ujson as json
except:
    import json

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../../')

from modules.monit.models.monit import Server, ServerData

# Return data of cheking alert, if first element is true, send hook. In servers, send all bad servers to webhook. 

def check_alert():

    db=WebModel.connection()

    z=0
    
    arr_server_disk={}

    #select * FROM test_table WHERE JSON_CONTAINS(JSON_EXTRACT(listener_descriptions, "$[*].Listener.SSLCertificateId"), '"arn:aws:acm:us-west-2:xxxx:certificate/xxx"');
    #AND JSON_EXTRACT(serverdata.data, '$.cpu_idle')>90;
    #DESKTOP-HLHPSSO       | 192.168.122.81  | 2021-06-23 23:50:11 | {"C:\\": [31619219456, 26451152896, 5168066560, 83.7], "D:\\": [6272772096, 6272772096, 0, 100.0]} | 174
    
    timestamp=datetime.obtain_timestamp(datetime.now(utc=True))-140
    
    date_last=datetime.timestamp_to_datetime(timestamp, sql_format_time='YYYY-MM-DD HH:mm:ss')

    with db.query('select server.hostname, serverdata.ip, serverdata.date, JSON_EXTRACT(serverdata.data, \'$.disks_info\') as disks_percent, server.id from server, serverdata WHERE serverdata.id IN (select MAX(id) from serverdata group by ip) AND serverdata.ip=server.ip AND serverdata.date>=%s group by serverdata.ip', [date_last]) as cursor:
        for data in cursor:
            
            """
            z+=1
            arr_server_high[data['hostname']]=data
            """
            #print(data)
            disks=json.loads(data['disks_percent'])
            
            for disk, disk_data in disks.items():
                
                if float(disk_data[3])>90 and float(disk_data[3])!=100:
                    
                    z+=1
                    disk_data['date']=str(disk_data['date'])
                    arr_server_disk[disk]=disk_data
                    

    ret=False

    if z>0:
        ret=True

    return ret, arr_server_disk

if __name__=='__main__':
    
    print(check_alert())
