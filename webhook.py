from modules.monit import monit_app, db
from flask import request, g
from settings import config
#from pymongo import MongoClient
from modules.monit.models.monit import Server, ServerData
#import datetime
from paramecio2.libraries import datetime
from paramecio2.libraries import sendmail
try:
    import ujson as json
    
except:
    
    import json

send=sendmail.SendMail(ssl=False)

if hasattr(config, 'email_from'):
    email_from=config.email_from

if hasattr(config, 'email_to'):
    email_to=config.email_to

if hasattr(config, 'email_user'):
    send.username=config.email_user

if hasattr(config, 'email_password'):
    send.password=config.email_password
    
if hasattr(config, 'email_host'):
    send.host=config.email_host

if hasattr(config, 'email_port'):
    send.port=config.email_port

@monit_app.route('/alerts', methods=['POST'])
def monit_webhook_alerts():
    
    #type_alert=request.form.get('type_alert')
    #data=request.form.get('data')ç
    data=request.get_json()
    
    subject='Alerta de servidor'
    
    #pre_alerts={'server_alert': ('Down server alert', 'modules.monit.alerts.servers'), 'cpu_alert': ('High CPU alert', 'modules.monit.alerts.cpu'), 'disk_alert': ('High Disk use', 'modules.monit.alerts.disks'), 'mem_alert': ('High mem alert', 'modules.monit.alerts.mem')}
    
    if data['type_alert']=='server_alert':
        
        # Set text email
        
        subject+=' Servidor caído'
        
        def text_email(hostname, data):
            
            return 'Alerta: servidor '+hostname+' con ip '+data_host['ip']+' está caído'
            
    elif data['type_alert']=='cpu_alert':
        
        subject+=' Uso de cpu alto'
        
        def text_email(hostname, data):
        
            return 'Alerta: servidor '+hostname+' con ip '+data_host['ip']+' tiene un uso de cpu muy alto '+data_host['cpu_idle']
    
    arr_msg=[]
    
    for hostname, data_host in data['data'].items():
        
        arr_msg.append(text_email(hostname, data))
        
    
    message="\n\n".join(arr_msg)
    
    if not send.send(email_from, email_to, subject, message, content_type='plain', attachments=[]):
        print(send.txt_error)
    
    send.quit()

    pass
        
    
    return {}
