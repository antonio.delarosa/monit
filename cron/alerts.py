import os
import sys

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../../')

from settings import config
from paramecio2.libraries.db.webmodel import WebModel
from modules.monit.models.monit import Alerts
from modules.monit.libraries.alerts import pre_alerts, get_alerts_config
import signal
from time import sleep
import importlib
import requests
try:
    import ujson as json
except:
    import json
from requests.exceptions import Timeout


def start():

    # Load things
    
    db=WebModel.connection()
    
    alerts=Alerts(db)
    
    db_alerts=alerts.select_to_array()
    
    db.close()
    
    arr_alerts={}

    get_alerts_config()
    
    for alert, data_alert in pre_alerts.items():
        #print(data_alert)
        arr_alerts[alert]=importlib.import_module(data_alert[1])
    
    # Import all alerts

    while True:
        
        for db_alert in db_alerts:
            
            alert_check=arr_alerts[db_alert['type_alert']]
            
            result_check, data=alert_check.check_alert()
            
            if result_check:
                
                print('Alert: sending '+db_alert['name']+' to webhook')
                
                try:
                    
                    json_data={'type_alert': db_alert['type_alert'], 'data': data}
                    
                    #data['type_alert']=db_alert['type_alert']
                    
                    r=requests.post(db_alert['webhook'], json=json_data, timeout=5)
                    
                except Timeout:
                    
                    print('The request timed out')
                
        sleep(60)

if __name__=='__main__':
    
    def catch_signal(sig, frame):
        print('Exiting...')
        exit(0)

    signal.signal(signal.SIGINT, catch_signal)

    start()
