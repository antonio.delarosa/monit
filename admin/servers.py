from settings import config
from flask import g, url_for, request
from paramecio2.libraries.generate_admin_class import GenerateAdminClass
from paramecio2.libraries.i18n import I18n
from paramecio2.modules.admin import admin_app, t as admin_t
from paramecio2.libraries.db.coreforms import SelectForm
from copy import copy
from paramecio2.libraries.mtemplates import env_theme
import os
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.libraries.db import coreforms
from settings import config
from pymongo import MongoClient
from bson.son import SON
#from datetime import datetime
from paramecio2.libraries import datetime
from paramecio2.libraries.urls import make_media_url
from modules.monit.models.monit import Server, ServerData
from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.lists import AjaxList
try:
    import ujson as json
except:
    import json

#t=copy(admin_t)

#t.env.directories.insert(0, os.path.dirname(__file__).replace('/admin', '')+'/templates')

env=env_theme(__file__)

t=PTemplate(env)

t.env.directories=admin_t.env.directories

t.env.directories.insert(1, os.path.dirname(__file__).replace('/admin', '')+'/templates/admin')

@admin_app.route('/admin/servers/', methods=['GET'])
def monit_servers():

    return t.load_template('servers.phtml', title=I18n.lang('monit', 'servers', 'Servers'), contents="", path_module='admin_app.monit_servers')


@admin_app.route('/admin/get_servers/', methods=['POST'])
def get_servers():
    """
    client = MongoClient(config.mongo_server, 27017)
    
    db = client['monit']

    servers_collection = db.get_collection('servers')
    
    arr_servers={}
    
    servers=servers_collection.find()
    
    for server in servers:
        
        arr_servers[server['ip']]=server
        
        del arr_servers[server['ip']]['_id']
    
    # Get last data with aggregate
    
    #db.data.aggregate([  { $sort: { datetime: 1 } },  {    $group:      {        _id: "$hostname",        lastDate: { $last: "$datetime" }      }  } ])
    
    options=[
        #{ '$sort': { SON([("_id": -1), ("datetime": -1)]) } },
        #{ "$sort": SON([("datetime", -1), ("_id", -1)])},
        { "$sort": {"datetime": 1} },
        {
            "$group":
            {
                "_id": "$ip",
                "last_cpu": { "$last": "$cpu_idle" },
                "last_date": { "$last": "$datetime" }
            }
        }
    ]
    
    servers_data = db.get_collection('data')
    
    #servers_data=servers_collection.find() #aggregate(options);
    
    arr_server_data={}
    
    for data in servers_data.aggregate(options):
    
    with db.query('select ')
    
    
        #arr_server_data[data['ip']=data
        #arr_servers[server['ip']]['datetime']=data['last_date']
    
        last_timestamp=int(datetime.timestamp(data['last_date']))
        timestamp=int(datetime.timestamp(datetime.utcnow()))-140
        
        arr_servers[data['_id']]['status']='<img src="'+make_media_url('images/status_green.png', 'monit')+'" />'
        
        if timestamp>last_timestamp:
            arr_servers[data['_id']]['status']='<img src="'+make_media_url('images/status_red.png', 'monit')+'" />'
    
        arr_servers[data['_id']]['cpu_info']=str(data['last_cpu'])+'%'
    
        arr_servers[data['_id']]['options']='<a href="#">View graphs</a>'
    
    
    return arr_servers
    """
    db=g.connection
    
    #s=session
    
    # [I18n.lang('leviathan', 'choose_server', 'Choose server'), False],
    
    fields=[[I18n.lang('monit', 'hostname', 'Hostname'), True], ['IP', True], [I18n.lang('monit', 'status', 'Status'), True], [I18n.lang('monit', 'cpu_use', 'CPU use'), False],  [I18n.lang('monit', 'options', 'Options'), False]]
    arr_order_fields=['hostname', 'ip', 'date']    
    
    count_query=['select count(server.id) as num_elements from server', []]
    
    # server.id as select_id,
    
    # select hostname, ip, date, num_updates, id from serverofuser where user_id=%s;
    
    str_query=['select server.hostname, serverdata.ip, serverdata.date, serverdata.data, server.id from server, serverdata WHERE serverdata.id IN (select MAX(id) from serverdata group by ip) AND serverdata.ip=server.ip group by serverdata.ip', []]
    
    ajax=AjaxList(db, fields, arr_order_fields, count_query, str_query)
    
    ajax.func_fields['id']=options_server
    ajax.func_fields['date']=options_status
    ajax.func_fields['ip']=options_ip
    ajax.func_fields['data']=options_data
    ajax.func_fields['options']=options_options
    ajax.limit=0
    
    #{'fields': [['Hostname', True], ['IP', True], ['Status', True], ['Options', False]], 'rows': [{'hostname': 'debian-pc.localdomain', 'ip': '<span id="ip_192.168.122.125">192.168.122.125</span>', 'date': '<img src="/mediafrom/monit/images/status_green.png" />', 'id': '<a href="#">View stats</a>'}, {'hostname': 'DESKTOP-HLHPSSO', 'ip': '<span id="ip_192.168.122.81">192.168.122.81</span>', 'date': '<img src="/mediafrom/monit/images/status_green.png" />', 'id': '<a href="#">View stats</a>'}], 'html_pages': ''}
    
    return ajax.show()

def options_server(row_id, row):
    
    #'<a href="{}">{}</a>'.format(url_for('.services', server_id=row_id), I18n.lang('leviathan', 'services', 'Services'))
    
    arr_options=['<a href="{}">{}</a>'.format(url_for('.view_stats', server_id=row_id), I18n.lang('leviathan', 'view_stats', 'View stats'))]
    
    return "<br />".join(arr_options)

def options_select_id(row_id, row):
    
    return '<input type="checkbox" name="server_id" id="server_id_%s" class="select_server" value="%s"/>' % (row_id, row_id)
    
def options_options(row_id, row):
    
    return '<a href="#">View stats</a>'

def options_status(value, row):
    
    now=datetime.now(True)
    
    timestamp_now=datetime.obtain_timestamp(now)
    
    value=str(value).replace('-', '').replace(':', '').replace(' ', '')
    
    timestamp_value=datetime.obtain_timestamp(value)

    five_minutes=int(timestamp_now)-200
    
    if timestamp_value<five_minutes:
        
        return '<img src="'+make_media_url('images/status_red.png', 'monit')+'" />'
    else:
        return '<img src="'+make_media_url('images/status_green.png', 'monit')+'" />'

def options_ip(value, row):
    
    return '<span id="ip_%s">%s</span>' % (value, value)
    
def options_data(value, row):
    
    data=json.loads(value)
    
    cpu_info=data['cpu_idle']
    
    return str(cpu_info)+'%'

@admin_app.route('/admin/update_status/', methods=['GET'])
def update_status():
    """
    client = MongoClient(config.mongo_server, 27017)
    
    db = client['monit']
    
    servers_data = db.get_collection('data')
    
    options=[
        #{ '$sort': { SON([("_id": -1), ("datetime": -1)]) } },
        #{ "$sort": SON([("datetime", -1), ("_id", -1)])},
        { "$sort": {"hostname":1, "datetime": 1} },
        {
            "$group":
            {
                "_id": "$ip",
                "last_cpu": { "$last": "$cpu_idle" },
                "last_date": { "$last": "$datetime" }
            }
        }
    ]
    
    arr_servers={}
    
    for data in servers_data.aggregate(options):
    
        last_timestamp=int(datetime.timestamp(data['last_date']))
        timestamp=int(datetime.timestamp(datetime.utcnow()))-140
        
        arr_servers[data['_id']]={}
        
        arr_servers[data['_id']]['status']='<img src="'+make_media_url('images/status_green.png', 'monit')+'" />'
        
        if timestamp>last_timestamp:
            arr_servers[data['_id']]['status']='<img src="'+make_media_url('images/status_red.png', 'monit')+'" />'
        
        arr_servers[data['_id']]['cpu_info']=str(data['last_cpu'])+'%'
    """
    
    db=g.connection
    
    arr_servers={}
        
    with db.query('select server.hostname, serverdata.ip, serverdata.date, JSON_EXTRACT(serverdata.data, \'$.cpu_idle\') as cpu_idle, server.id from server, serverdata WHERE serverdata.id IN (select MAX(id) from serverdata group by ip) AND serverdata.ip=server.ip group by serverdata.ip', []) as cursor:
        for data in cursor:
            #{'hostname': 'debian-pc.localdomain', 'ip': '192.168.122.125', 'date': datetime.datetime(2021, 6, 21, 15, 54, 13), 'data': '{"net_info":[269270,491053,2993,7543,0,0,4,0],"cpu_idle":1.5,"cpus_idle":[2.0,3.0],"cpu_number":2,"disks_info":{"\\/":[20042096640,1827229696,17173159936,9.6]},"mem_info":[1034973184,687661056,33.6,204156928,661594112,207618048,68468736,13058048,156164096,3080192,38555648],"hostname":"debian-pc.localdomain"}', 'id': 6}
            
            #last_timestamp=int(datetime.timestamp(data['last_date']))
            #timestamp=int(datetime.timestamp(datetime.utcnow()))-140
            last_timestamp=datetime.obtain_timestamp(str(data['date']).replace('-', '').replace(':', '').replace(' ', ''))
            timestamp=datetime.obtain_timestamp(datetime.now(utc=True))-140
            
            arr_servers[data['ip']]={}
            
            arr_servers[data['ip']]['status']='<img src="'+make_media_url('images/status_green.png', 'monit')+'" />'
            
            if timestamp>last_timestamp:
                arr_servers[data['ip']]['status']='<img src="'+make_media_url('images/status_red.png', 'monit')+'" />'
            
            arr_servers[data['ip']]['cpu_info']=str(data['cpu_idle'])+'%'
            
            arr_servers[data['ip']]['disk_percent']=''
            
            
    return arr_servers
    
@admin_app.route('/admin/get_net_data/<server_id>')
def get_net_data(server_id):
    
    #s=session

    db=g.connection
    
    server=Server(db)
    
    arr_server=server.set_conditions('WHERE id=%s', [server_id]).select_a_row_where()
    
    if arr_server:
        
        if 'ip' in arr_server:
            
            ip=arr_server['ip']
            
            now=datetime.obtain_timestamp(datetime.now(utc=True))
            
            hours12=now-3660
            
            date_now=datetime.timestamp_to_datetime(now, sql_format_time='YYYY-MM-DD HH:mm:ss')
            
            date_hours12=datetime.timestamp_to_datetime(hours12, sql_format_time='YYYY-MM-DD HH:mm:ss')
            
            #serverdata=ServerData(db)
            
            arr_cpu=[]
            
            arr_final=[]
            
            with db.query('select * from serverdata where ip=%s AND date>=%s AND date<=%s', [ip, date_hours12, date_now]) as cursor:
                
                first_row=cursor.fetchone()
                
                if first_row:
                    
                    data_net=json.loads(first_row['data'])
                    
                    first_sent=data_net['net_info'][0]
                    first_recv=data_net['net_info'][1]
                    
                else:
                    
                    first_recv=0
                    first_sent=0
                
                for row in cursor:
                    data=json.loads(row['data'])
                    
                    bytes_sent=round((data['net_info'][0]-first_sent)/1024)
                    bytes_recv=round((data['net_info'][1]-first_recv)/1024)
                    
                    first_sent=data['net_info'][0]
                    first_recv=data['net_info'][1]
                    
                    mem_used=((data['mem_info'][3]/1024)/1024)/1024
                    mem_free=((data['mem_info'][4]/1024)/1024)/1024
                    
                    try:
                    
                        mem_cached=((data['mem_info'][8]/1024)/1024)/1024
                        
                    except:
                        mem_cached=0
                        
                    
                    arr_final.append({'cpu': data['cpu_idle'], 'bytes_sent': bytes_sent, 'bytes_recv': bytes_recv, 'date': str(row['date']), 'memory_used': mem_used, 'memory_free': mem_free, 'memory_cached': mem_cached})
                    
                    
            return json.dumps(arr_final)
        
        return json.dumps({})

    return {}
    
@admin_app.route('/admin/get_disk_data/<server_id>')
def get_disk_data(server_id):
    
    """
    for disk, data in arr_info['disks_info'].items():
        
        status_disk.set_conditions('where ip=%s and disk=%s', [ip, disk])
        
        method_update({'ip' : ip, 'disk' : disk, 'date' : now, 'size' : data[0], 'used' : data[1], 'free' : data[2], 'percent' : data[3]})
    """
    
    db=g.connection
            
    now=datetime.obtain_timestamp(datetime.now(utc=True))
    
    hours12=now-3660
    
    date_now=datetime.timestamp_to_datetime(now, sql_format_time='YYYY-MM-DD HH:mm:ss')
    
    date_hours12=datetime.timestamp_to_datetime(hours12, sql_format_time='YYYY-MM-DD HH:mm:ss')
    
    server=Server(db)
    
    arr_server=server.set_conditions('WHERE id=%s', [server_id]).select_a_row_where()
    
    #{'net_info': [140298240, 882661734, 523317, 951538, 0, 0, 0, 0], 'cpu_idle': 0.0, 'cpus_idle': [0.0, 0.0], 'cpu_number': 2, 'disks_info': {'C:\\': [31619219456, 26415333376, 5203886080, 83.5], 'D:\\': [6272772096, 6272772096, 0, 100.0]}, 'mem_info': [4294422528, 2473185280, 42.4, 1821237248, 2473185280], 'hostname': 'DESKTOP-HLHPSSO'}
    
    if arr_server:
        
        if 'ip' in arr_server:
            
            ip=arr_server['ip']
            
            with db.query('select data from serverdata WHERE ip=%s order by date DESC limit 1', [ip]) as cursor:
                
                row=cursor.fetchone()
                data=json.loads(row['data'])
                
                arr_disk=[]
                
                for disk, info in data['disks_info'].items():
                    arr_disk.append({'disk': disk, 'used': info[1], 'free': info[2], 'percent': info[3]})
                
                return json.dumps(arr_disk)
                    
                """
                arr_disk=status_disk.select_to_array(['disk', 'used', 'free', 'date'])
        
                return json.dumps(arr_disk)
                """
    
    return {}


@admin_app.route('/admin/view_stats/<server_id>', methods=['GET'])
def view_stats(server_id):

    db=g.connection
    
    server=Server(db)
    
    arr_server=server.set_conditions('WHERE id=%s', [server_id]).select_a_row_where()

    return t.load_template('view_stats.phtml', title=I18n.lang('monit', 'stats', 'Stats'), contents="", path_module='admin_app.monit_servers', server=arr_server)

"""
@admin_app.route('/admin/alerts')
def monit_alerts():
    
    return t.load_template('alerts.phtml', title=I18n.lang('monit', 'alerts', 'Alerts'), contents="", path_module='admin_app.alerts')
    
"""
