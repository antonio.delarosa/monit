from settings import config
from flask import g, url_for, request
from paramecio2.libraries.generate_admin_class import GenerateAdminClass
from paramecio2.libraries.i18n import I18n
from paramecio2.modules.admin import admin_app, t as admin_t
from paramecio2.libraries.db.coreforms import SelectForm
from copy import copy
from paramecio2.libraries.mtemplates import env_theme
import os
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.libraries.db import coreforms
from settings import config
from pymongo import MongoClient
from bson.son import SON
#from datetime import datetime
from paramecio2.libraries import datetime
from paramecio2.libraries.urls import make_media_url
from modules.monit.models.monit import Server, ServerData
from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.lists import AjaxList
try:
    import ujson as json
except:
    import json

#t=copy(admin_t)

#t.env.directories.insert(0, os.path.dirname(__file__).replace('/admin', '')+'/templates')

env=env_theme(__file__)

t=PTemplate(env)

t.env.directories=admin_t.env.directories

t.env.directories.insert(1, os.path.dirname(__file__).replace('/admin', '')+'/templates/admin')

@admin_app.route('/admin/config')
def monit_config():

    return t.load_template('config.phtml', title=I18n.lang('monit', 'config', 'Configuration'), contents="", path_module='admin_app.monit_config')
    
