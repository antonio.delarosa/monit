from modules.monit import monit_app, db
from flask import request, g
from settings import config
#from pymongo import MongoClient
from modules.monit.models.monit import Server, ServerData
#import datetime
from paramecio2.libraries import datetime
try:
    import ujson as json
    
except:
    
    import json

"""Simple flask method for provide a endpoint with api key for set data values from a server

Args:
    api_key (str): The api key for get access to this endpoint
    
Returns:

   dict: Return a dict with error key setting if have error
"""

@monit_app.route('/get_data/<api_key>', methods=['POST'])
@db
def monit_get_data(api_key):

    connection=g.connection

    ip=request.remote_addr

    error=1
    txt_error='Error: api key incorrect'
    
    if config.monit_api_key==api_key:
        error=0
        txt_error=''

    #print(request.form.get('data_json'))
    #{"net_info": [296348632, 7539805828, 2719721, 5566518, 0, 0, 0, 0], "cpu_idle": 5.3, "cpus_idle": [0.0, 0.0], "cpu_number": 2, "disks_info": {"C:\\": [31619219456, 26281410560, 5337808896, 83.1], "D:\\": [6272772096, 6272772096, 0, 100.0]}, "mem_info": [4294422528, 2388201472, 44.4, 1906221056, 2388201472]}
    
    if error==0:
    
        data_json=json.loads(request.form.get('data_json'))
    
        server=Server(connection)
        #server.safe_query()
        #server.insert({'hostname': data_json['hostname'], 'ip': ip})
        server.query('INSERT IGNORE INTO server (`hostname`, `ip`) VALUES (%s, %s)', [data_json['hostname'], ip])
    
        now=datetime.format_local_strtime('YYYY-MM-DD HH:mm:ss', datetime.now(utc=True))
    
        server.query('INSERT INTO serverdata (`date`, `ip`, `data`) VALUES (%s, %s, %s)', [now, ip, json.dumps(data_json)])
        
    """
    client = MongoClient(config.mongo_server, 27017)
    
    db = client['monit']

    servers_collection = db.get_collection('servers')

    key = {'ip': ip}
    data = {'$set': {'hostname': data_json['hostname'], 'ip': ip}}
    
    servers_collection.update_one(key, data, upsert=True)
    
    data_collection = db.get_collection('data')

    data_json['ip']=ip
    
    data_json['datetime']=datetime.datetime.utcnow()
    
    data_collection.insert_one(data_json)
    """

    return {'error': error, 'txt_error': txt_error}
    
@monit_app.route('/admin/alert/<api_key>/<path:alert>', methods=['POST'])
def monit_alert(alert):
    
    connection=g.connection
    
    # Save in alertlog and send to endpoint (Normally Node-RED)
    ip=request.remote_addr

    error=1
    txt_error='Error: api key incorrect'
    
    if config.monit_api_key==api_key:
        error=0
        txt_error=''
        
    
    
    pass
